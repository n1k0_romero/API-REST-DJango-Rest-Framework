from django.contrib import admin
from .models import Empleado


@admin.register(Empleado)
class ServicioAdmin(admin.ModelAdmin):
    list_display = ('id', 'gmin', 'email')
    list_display_links = ('id', 'gmin', 'email')

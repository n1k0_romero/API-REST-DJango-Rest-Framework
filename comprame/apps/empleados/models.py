from django.db import models
from django.contrib.auth.models import User


class Empleado(models.Model):
    gmin = models.CharField(max_length=5)
    email = models.EmailField()
    user = models.ForeignKey(User, null=True, blank=True)

    def __str__(self):
        return '%s - %s' % (self.gmin, self.email)
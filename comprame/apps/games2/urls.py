from django.conf.urls import url
from comprame.apps.games2 import views as games_views


urlpatterns = [
    url(r'^games/2/$', games_views.game_list, name='game_list'),
    url(r'^games/2/(?P<pk>[0-9]+)/$', games_views.game_detail, name='game_detail'),
]
from django.contrib import admin
from comprame.apps.games.models import Game

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = ('id', 'created', 'name', 'game_category', 'played')
    list_display_links = ('id', 'created', 'name', 'game_category', 'played')


from django.conf.urls import url
from comprame.apps.games import views as games_views


urlpatterns = [
    url(r'^games/$', games_views.game_list, name='game_list'),
    url(r'^games2/(?P<pk>[0-9]+)/$', games_views.game_detail, name='game_detail'),
]
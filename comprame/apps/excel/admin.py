from import_export import resources
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from comprame.apps.excel.models import Book


class BookResource(resources.ModelResource):

    class Meta:
        model = Book
        fields = ('author__name', 'name', 'price')


@admin.register(Book)
class BookAdmin(ImportExportModelAdmin):
    resource_class = BookResource



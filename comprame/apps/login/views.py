from django.views.generic import TemplateView,View
from django.http import HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.contrib.auth.models import User
from comprame.apps.empleados.models import Empleado


class LoginView(TemplateView):
    template_name = 'login/login.html'


class AccesoView(View):

    def post(self, request):
        gmin = request.POST['gmin']
        email = request.POST['email']
        try:
            empleado = Empleado.objects.get(gmin=gmin, email=email)
        except empleado.DoesNotExist:
            print("No esxiste el empleado")
        usuario_empleado = User.objects.get(id=empleado.user.id)
        usuario_empleado.backend='django.contrib.auth.backends.ModelBackend'
        login(request, usuario_empleado)
        username = usuario_empleado.username
        password = usuario_empleado.password
        usuario = authenticate(username=username, password=password)

        if usuario is not None:
            print('entra aqui')
            if usuario.is_active:
                login(request, usuario)
                return HttpResponseRedirect(reverse_lazy('example1'))
            else:
                messages.error(self.request, 'El usuario <b>%s</b> no esta activo.' % username, extra_tags='safe')
                return HttpResponseRedirect(reverse_lazy('inicio'))
        else:
            messages.error(self.request, 'No se encontro el usuario <b>%s</b>.' % username, extra_tags='safe')
            return HttpResponseRedirect(reverse_lazy('example1'))


from django.conf.urls import url
from .views import LoginView, AccesoView


urlpatterns = [
    url(r'^inicio/$', LoginView.as_view(), name='login'),
    url(r'^accesar$', AccesoView.as_view(), name='accesar'),
]
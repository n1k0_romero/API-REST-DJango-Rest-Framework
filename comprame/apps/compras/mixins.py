from django.db import models
from datetime import datetime
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets, filters, status
from rest_framework.response import Response


class ManagerMain(models.Manager):

    def get_queryset(self):
        return super(ManagerMain, self).get_queryset().filter(deleted_at__isnull=True)


class ManagerAllMain(models.Manager):

    def get_queryset(self):
        return super(ManagerAllMain, self).get_queryset()


class TimeStampedModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    objects = ManagerMain()
    objects_all = ManagerAllMain()

    class Meta:
        abstract = True


class DefaultViewSetMixin(object):
    """ Mixin que define los filtros busquedas y paginaciones por defecto. """

    authentication_classes = (SessionAuthentication, TokenAuthentication, )
    permission_classes = (IsAuthenticated, )
    filter_backends = (filters.OrderingFilter, filters.SearchFilter, )
    paginate_by = 25
    paginate_by_param = 'page_size'
    max_paginate_by = 100


class ModelViewSetMixin(viewsets.ModelViewSet):
    """ Mixin que realiza el borrado logico de un objeto y guarda la hora de eliminacion. """

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.deleted_at = datetime.now()
        instance.save()
        response = {
            "Borrado logico ok"
        }
        return Response(response, status=status.HTTP_204_NO_CONTENT)


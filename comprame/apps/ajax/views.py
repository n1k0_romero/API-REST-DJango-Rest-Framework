from django.views.generic import TemplateView


class FooterView(TemplateView):
    template_name = 'ajax/footer.html'


class AjaxExample1View(TemplateView):
    template_name = 'ajax/ajax_example1.html'


class AjaxExample2View(TemplateView):
    template_name = 'ajax/ajax_example2.html'


class AjaxDataJson(TemplateView):
    template_name = 'ajax/data.JSON'


class AjaxExample3View(TemplateView):
    template_name = 'ajax/ajax_example3.html'
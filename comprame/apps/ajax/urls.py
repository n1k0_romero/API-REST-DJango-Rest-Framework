from django.conf.urls import url
from comprame.apps.ajax.views import AjaxExample1View, FooterView, AjaxExample2View, AjaxDataJson, AjaxExample3View


urlpatterns = [
    url(r'^footer/$', FooterView.as_view(), name='footer'),
    url(r'^example1/$', AjaxExample1View.as_view(), name='example1'),
    url(r'^example2/$', AjaxExample2View.as_view(), name='example2'),
    url(r'^dataJson/$', AjaxDataJson.as_view(), name='data_json'),
    url(r'^example3/$', AjaxExample3View.as_view(), name='example3'),
]
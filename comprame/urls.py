"""comprame URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from comprame.apps.compras.views import ClientViewSet, ProductViewSet, PurchaseViewSet, PurchaseItemViewSet,\
    AuthLoginView

router = routers.DefaultRouter()
router.register(r'clients', ClientViewSet)
router.register(r'product', ProductViewSet)
router.register(r'purchase', PurchaseViewSet)
router.register(r'purchase_items', PurchaseItemViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^api/', include('comprame.apps.games.urls')),
    url(r'^api2/', include('comprame.apps.games2.urls')),
    url(r'^ajax/', include('comprame.apps.ajax.urls')),
    url(r'^', include('comprame.apps.login.urls')),
    url(r'^devices/login', AuthLoginView.as_view(), name='devices_login'),
]
